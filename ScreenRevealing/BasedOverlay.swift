//
//  BasedOverlay.swift
//  ScreenRevealing
//
//  Created by Raileanu, Catalin on 9/9/16.
//  Copyright © 2016 Hazard Creation. All rights reserved.
//

import UIKit

class BasedOverlay: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        layer.shadowColor = UIColor.lightGrayColor().CGColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSizeZero
        layer.shadowRadius = 2.0
    }
    
    func dismiss() {
        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.transform = CGAffineTransformMakeTranslation(0.0, self.frame.height)
        }) { (finished) -> Void in
            self.removeFromSuperview()
        }
    }

}
