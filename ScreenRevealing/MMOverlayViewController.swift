//
//  MMOverlayViewController.swift
//  ScreenRevealing
//
//  Created by Raileanu, Catalin on 9/6/16.
//  Copyright © 2016 Hazard Creation. All rights reserved.
//

import UIKit

class MMOverlayViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //
    }
}
