//
//  ViewController.swift
//  ScreenRevealing
//
//  Created by Raileanu, Catalin on 8/22/16.
//  Copyright © 2016 Hazard Creation. All rights reserved.
//

import UIKit

let imageTransparencyTresholder:CGFloat = 30.0
let brushShadow:CGFloat = 20.0
let timeCounter = 2
let animationViewShadowRadius:CGFloat = 2.0
let brushWidth: CGFloat = 50.0
let opacity: CGFloat = 1.0
let mileageEarned = 1394

class ViewController: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var mileageCountingLabel: HMTAccountStatementCounter!
    @IBOutlet weak var doNotLetGoLabel: UILabel!
    
    var lastPoint = CGPoint.zero
    var counter = timeCounter;
    var defrostTimer = NSTimer()
    var screenRevealed = false
    
    lazy var overlayViewController:MMOverlayViewController = {
        let overlayViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MMOverlayStoryboardID") as! MMOverlayViewController
        return overlayViewController
    }()
    
    lazy var actionView:ActionOverlayView = {
        
        let customView = NSBundle.mainBundle().loadNibNamed("ActionOverlayView", owner: self, options: nil).first as! ActionOverlayView

        let rect = CGRect.init(x: 0.0, y: self.view.frame.height+animationViewShadowRadius,
                               width: self.view.frame.size.width, height: customView.frame.height)
        customView.frame = rect
        customView.collectNowButton.addTarget(self, action: #selector(collectNowButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)

        return customView
    }()
    
    lazy var partnerView:PartnerOverlayView = {
        
        let customView = NSBundle.mainBundle().loadNibNamed("PartnerOverlayView", owner: self, options: nil).first as! PartnerOverlayView
        
        let rect = CGRect.init(x: 0.0, y: self.view.frame.height+animationViewShadowRadius,
                                         width: self.view.frame.size.width, height: customView.frame.height)
        customView.frame = rect
        
        return customView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mileageCountingLabel.setValue(mileageEarned)
        
        doNotLetGoLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        doNotLetGoLabel.layer.shadowOpacity = 1
        doNotLetGoLabel.layer.shadowRadius = 2
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       self.presentViewController(self.overlayViewController, animated: false, completion: nil)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if screenRevealed == true {
            return
        }
        defrostTimer.invalidate()
        if let touch = touches.first {
            lastPoint = touch.locationInView(self.view)
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {

        if screenRevealed == true {
            return
        }
        if let touch = touches.first {
            let currentPoint = touch.locationInView(view)
            drawLineFrom(lastPoint, toPoint: currentPoint)
            lastPoint = currentPoint
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {

        if screenRevealed == true {
            return
        }

        doNotLetGoLabel.fadeIn()
        defrostTimer = NSTimer.scheduledTimerWithTimeInterval(0.7, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        counter = timeCounter
        if let touch = touches.first {
            let currentPoint = touch.locationInView(view)
            drawLineFrom(lastPoint, toPoint: currentPoint)
            lastPoint = currentPoint
            let screenTransparency = getTransparencyForImage(tempImageView.image!)
            if screenTransparency > imageTransparencyTresholder {
                defreezeScreen()
            }
        }
    }
    
    func getTransparencyForImage(image:UIImage) -> CGFloat {
        let pixelData = CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage))
        let data = CFDataGetBytePtr(pixelData)
        
        let width = (Int)(image.size.width)
        let height = (Int)(image.size.height)
        var transparentPixels:CGFloat = 0
        
        for x in 0..<width {
            for y in 0..<height {
                let pixelInfo:Int = ((width * y) + x) * 4
                let alpha = CGFloat(data[pixelInfo+3])
                if alpha == 0 {
                    transparentPixels += 1
                }
            }
        }
        return (transparentPixels*100.0)/(image.size.width*image.size.height)
    }
    
    func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {
        
        UIGraphicsBeginImageContext(view.frame.size)
        let context = UIGraphicsGetCurrentContext()
        tempImageView.image?.drawInRect(CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))

        CGContextMoveToPoint(context, fromPoint.x, fromPoint.y)
        CGContextAddLineToPoint(context, toPoint.x, toPoint.y)
        
        CGContextSetLineCap(context, CGLineCap.Round)
        CGContextSetLineWidth(context, brushWidth)
        CGContextSetShadow(context, CGSizeMake(0.0, 0.0), brushShadow)

        CGContextSetBlendMode(context, CGBlendMode.Clear)
        CGContextStrokePath(context)
        
        tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        tempImageView.alpha = opacity
        UIGraphicsEndImageContext()
            
    }

    func update () -> Void {
        if counter == 0 {
            defrostTimer.invalidate()
            freezeScreen()
        } else {
            counter -= 1
        }
    }
    
    func freezeScreen() {
        crossFadeImage(tempImageView.image!, toImage: UIImage(named: "snowBKG")!)
        doNotLetGoLabel.fadeOut()
    }
    
    func defreezeScreen() {
        crossFadeImage(tempImageView.image!, toImage: UIImage(named: "christmasBKG")!)
        defrostTimer.invalidate()
        screenRevealed = true
        self.view.addSubview(actionView)
        self.performSelector(#selector(showOverlay), withObject: actionView, afterDelay: 1.0)
        doNotLetGoLabel.fadeOut()
    }

    func collectNowButtonPressed() {
        mileageCountingLabel.animateValue(2.0, max: mileageEarned) {
            self.view.addSubview(self.partnerView)
            self.showOverlay(self.partnerView)
        }
        actionView.dismiss()
    }
    
    func crossFadeImage(fromImage:UIImage, toImage:UIImage) -> Void {
        let crossFade:CABasicAnimation = CABasicAnimation(keyPath: "contents")
        crossFade.duration = 1.0
        crossFade.fromValue = fromImage.CGImage
        crossFade.toValue = toImage.CGImage
        tempImageView.layer.addAnimation(crossFade, forKey:"animateContents")
        tempImageView.image = toImage

    }
   
    func showOverlay(overlay:BasedOverlay) -> Void {
        UIView.animateWithDuration(1.0) {
            overlay.transform = CGAffineTransformMakeTranslation(0.0, -overlay.bounds.height)
        }
    }

}

