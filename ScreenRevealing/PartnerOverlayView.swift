//
//  PartnerOverlayView.swift
//  ScreenRevealing
//
//  Created by Raileanu, Catalin on 9/8/16.
//  Copyright © 2016 Hazard Creation. All rights reserved.
//

import UIKit

class PartnerOverlayView: BasedOverlay {

    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var partnerButton: UIButton!
}
