//
//  StringExtensions.swift
//  miles-more
//
//  Created by Wojciech Charysz on 03.11.15.
//  Copyright © 2015 hmmh multimediahaus AG. All rights reserved.
//

import Foundation

public extension String {
    public subscript (i: Int) -> String {
        return self.substringWithRange(self.startIndex..<self.startIndex.advancedBy(i + 1))
    }
    
    public subscript (r: Range<Int>) -> String {
        get {
            return self.substringWithRange(self.startIndex.advancedBy(r.startIndex)..<self.startIndex.advancedBy(r.endIndex))
        }
    }
    
    var localized: String {
        return NSLocalizedStringWithDefault(self, comment: "")
    }
    
    var localizedUrlString: String {
        return NSLocalizedString(self, tableName: "Urls", bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
    
    func NSLocalizedStringWithDefault (key:String, comment:String)->String {
        let message = NSLocalizedString(key, comment: comment)
        if message != key {
            return message
        }
        let language = "en"
        
        if let path = NSBundle.mainBundle().pathForResource(language, ofType: "lproj"), let bundle = NSBundle(path: path) {
            return bundle.localizedStringForKey(key, value: nil, table: nil)
        } else {
            return key
        }
    }
    
    
    func replace(string:String, replacement:String) -> String {
        return self.stringByReplacingOccurrencesOfString(string, withString: replacement, options: .LiteralSearch, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
}
