//
//  HMTDisplayLink.swift
//  miles-more
//
//  Created by Kaweh Kazemi on 05.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import Foundation
import UIKit


public class HMTDisplayLink {
    private var updateCallback: ((CFTimeInterval) -> Void)? = nil
    private var displayLink: CADisplayLink? = nil
    private var previousTimestamp: CFTimeInterval = -1
    
    deinit {
        updateCallback = nil
        displayLink?.invalidate()
        displayLink = nil
    }
    
    public func setup(runImmediatly: Bool = false, update: (CFTimeInterval) -> Void) {
        updateCallback = update
        if runImmediatly {
            start()
        }
    }
    
    public func start() {
        stop()
        displayLink = CADisplayLink(target: self, selector: #selector(HMTDisplayLink.tick))
        displayLink?.addToRunLoop(NSRunLoop.mainRunLoop(), forMode: NSDefaultRunLoopMode)
    }
    
    public var speed: Int {
        get {
            return displayLink?.frameInterval ?? 1
        }
        set {
            displayLink?.frameInterval = newValue
        }
    }
    
    public var paused: Bool {
        get {
            return displayLink?.paused ?? false
        }
    }
    
    public func pause() {
        displayLink?.paused = true
    }
    
    public func `continue`() {
        displayLink?.paused = false
    }
    
    public func stop() {
        displayLink?.invalidate()
        displayLink = nil
        previousTimestamp = -1
    }
    
    @objc public func tick() {
        guard let displayLink = displayLink else { return }
        guard let updateCallback = updateCallback else { return }
        let now = displayLink.timestamp
        let dt = now - (previousTimestamp < 0 ? now : previousTimestamp)
        previousTimestamp = now
        updateCallback(dt)
    }
}
