//
//  UIView+Extensions.swift
//  miles-more
//
//  Created by Julien Klindt on 19.11.15.
//  Copyright © 2015 hmmh multimediahaus AG. All rights reserved.
//

import UIKit
//import SnapKit

public extension UIView
{
    
    public func fadeIn(duration: NSTimeInterval = 1.0, delay: NSTimeInterval = 0.0) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.alpha = 1.0
            }, completion: nil)
    }
    
    public func fadeOut(duration: NSTimeInterval = 1.0, delay: NSTimeInterval = 0.0) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.alpha = 0.0
            }, completion: nil)
    }
    
    public func addBackgroundParallax(strength:CGFloat)
    {
        addParallax(-strength)
    }
    
    
    public func addForegroundParallax(strength:CGFloat)
    {
        addParallax(strength)
    }
    
    
    public func removeParallax()
    {
        for effect in self.motionEffects
        {
            removeMotionEffect(effect)
        }
    }
    
    public func addBlur() -> UIView {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        addSubview(blurEffectView)
        
        return blurEffectView
    }
    
    public func removeBlur() {
        for view in self.subviews where view is UIVisualEffectView {
            if let effectView = view as? UIVisualEffectView, let effect = effectView.effect where effect is UIBlurEffect {
                effectView.removeFromSuperview()
            }
        }
    }
    
    public func addParallax(amount:CGFloat)
    {
        removeParallax()
        
        let verticalMotionEffect:UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = -amount
        verticalMotionEffect.maximumRelativeValue = amount
        
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = -amount
        horizontalMotionEffect.maximumRelativeValue = amount
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        self.addMotionEffect(group)
    }
    
    /**
     * Taking screenshot of specified region of the UIView
     */
    public func takeSnapshot(rect: CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(UIScreen.mainScreen().bounds.size, false, UIScreen.mainScreen().scale)
        if drawViewHierarchyInRect(UIScreen.mainScreen().bounds, afterScreenUpdates: true) {
            let image = UIGraphicsGetImageFromCurrentImageContext()
            let crop = CGRect(x: image.scale*rect.origin.x, y: image.scale*rect.origin.y, width: image.scale*rect.size.width, height: image.scale*rect.size.height)
            if let imageRef = CGImageCreateWithImageInRect(image.CGImage, crop) {
                let screen = UIImage(CGImage: imageRef, scale: image.scale, orientation: .Up)
                UIGraphicsEndImageContext()
                return screen
            }
        }
        UIGraphicsEndImageContext()
        return nil
    }
}