//
//  HMTAccountStatementCounter.swift
//  miles-more
//
//  Created by Kaweh Kazemi on 25.04.16.
//  Copyright © 2016 hmmh multimediahaus AG. All rights reserved.
//

import Foundation
import UIKit


class HMTAccountStatementCounter: UILabel {

    var displayLink = HMTDisplayLink()
    var completionHandler: (()->Void)? = nil
    
    var currentValue: Int = 0
    var maxValue: Int = 0
    var currentValueExact: Double = 0
    var animationDuration: CFTimeInterval = 0


    private func setTextValue(value: Int) {
        dispatch_async(dispatch_get_main_queue()) {
            self.text = ConstFunc.milesNumberFormatter.stringForObjectValue(value)
        }
    }
    
    func setValue(value: Int) {
        displayLink.stop()
        setTextValue(value)
    }
    
    func animateValue(duration: CFTimeInterval, max: Int, minValue: Int? = nil, completion: (()->Void)?) {
        currentValue = minValue ?? 0
        maxValue = max
        currentValueExact = Double(currentValue)
        animationDuration = duration
        setTextValue(currentValue)
        completionHandler = completion
        displayLink.setup(true, update: updateValue)
    }
    
    func updateValue(dt: CFTimeInterval) {
        currentValueExact = min(Double(maxValue), currentValueExact + dt*CFTimeInterval(maxValue)/animationDuration)
        let newValue = Int(floor(currentValueExact))
        if newValue != currentValue {
            currentValue = newValue
            setTextValue(currentValue)
        }
        if currentValue >= maxValue {
            displayLink.stop()
            completionHandler?()
        }
    }
    
}
