//
//  ConstFunc.swift
//  miles-more
//
//  Created by Julien Klindt on 21.10.15.
//  Copyright © 2015 hmmh multimediahaus AG. All rights reserved.
//

import UIKit


struct ConstFunc {
    
    static func deviceIsIphone4() -> Bool {
        return DeviceType.IS_IPHONE_4_OR_LESS
    }
    
    static func deviceIsIphone5() -> Bool {
        return DeviceType.IS_IPHONE_5
    }
    
    static func deviceIsIphone6() -> Bool {
        return DeviceType.IS_IPHONE_6
    }

    static func deviceIsIphone6Plus() -> Bool {
        return DeviceType.IS_IPHONE_6P
    }
    
    static private let formattedNumber = NSNumberFormatter()
    static var milesNumberFormatter: NSNumberFormatter {
        ConstFunc.formattedNumber.numberStyle = .DecimalStyle
        ConstFunc.formattedNumber.groupingSeparator = "numberFormat".localized
        ConstFunc.formattedNumber.maximumFractionDigits = 0
        return ConstFunc.formattedNumber
    }
    
    static func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))),
            dispatch_get_main_queue(),
            closure
        )
    }
    
    static func radiansToDegrees(radians: CGFloat) -> CGFloat {
        return radians*180.0/CGFloat(M_PI)
    }
    
    static func degreesToRadians(degrees: CGFloat) -> CGFloat {
        return degrees/180.0*CGFloat(M_PI)
    }
    
    
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    static func statusBarHeightExpand() -> CGFloat {
        let statusBarSize = UIApplication.sharedApplication().statusBarFrame.size
        let height = min(statusBarSize.width, statusBarSize.height)
        if height > 20 {
            return height - 20
        }
        return 0
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    }
    
    
    static func getLanguageCode() -> String {
        if let localeIdentifier = NSLocale.preferredLanguages().first, let languageCode = NSLocale.componentsFromLocaleIdentifier(localeIdentifier)[NSLocaleLanguageCode] {
            return languageCode
        }
        else {
            return "en"
        }
    }
    
    static func shouldHideBars(scrollView: UIScrollView) -> Bool {
        if scrollView.decelerating {
            return scrollView.contentOffset.y > 90
        }
        else {
            return scrollView.contentOffset.y < 0 || scrollView.contentOffset.y > 90
        }
    }
}
